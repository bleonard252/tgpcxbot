# Telegram Public Channel Exposure Bot
This bot exposes 2 API endpoints:
* `/channel/:channelid/messages`: Get a list of messages in Telegram format.
* `/channel/:channelid/socket`: Get messages as they come in, in Telegram format.

Since the official bot is hosted for free on Heroku, it is currently not usable outside of its creator's intended purposes. Thus, you cannot add the bot to any channels.