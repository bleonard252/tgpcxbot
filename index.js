const { Telegraf } = require('telegraf');
var express = require('express');
var debughttp = require('debug')('http');
var debugbot = require('debug')('telegram');

const bot = new Telegraf(process.env.BOT_TOKEN);
bot.start((ctx) => ctx.reply('This is an internal bot for now'));
bot.help((ctx) => ctx.reply('This is an internal bot for now'));
//bot.on('sticker', (ctx) => ctx.reply('👍'))
//bot.hears('hi', (ctx) => ctx.reply('Hey there'))
bot.startPolling(5);//30,100,null,() => {debugbot("Polling stopped!")});

var app = express();
app.get('/', function (req, res) {
    res.type('html');
    res.send(/*html*/`<!DOCTYPE html>
<head>
    <title>API Help</title>
</head>
<body>
    <ul>
        <li>
            <code>/channel/:channelid/messages</code>: Get a list of messages in Telegram format.
        </li>
        <li>
            <code>/channel/:channelid/socket</code>: Get messages as they come in, in Telegram format.
        </li>
    </ul>
</body>`);
});

app.get('/channel/:channelid/messages', async function (req, res, next) {
    var output = {"messages": [], "other": []};
    var updates = await bot.telegram.getUpdates();
    for (var update in updates) {
        if (update.channel_post 
            && update.channel_post.chat.id == req.params["channelid"]) {
            //&& !update.channel_post.text.startsWith("/")) {
            output.messages.push(update.channel_post);
        } else {output.other.push(update)}
    }
    debughttp(output);
    res.type('json');
    res.send(JSON.stringify(output));
});

debughttp(`Listening on port ${process.env.PORT || 3000}`);
app.listen(process.env.PORT || 3000);